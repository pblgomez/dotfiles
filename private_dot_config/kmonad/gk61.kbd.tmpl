(defcfg
  {{ if eq .chezmoi.os "linux" -}}
  input  (device-file "/dev/tmkb-gk61")
  output (uinput-sink "kmonad-gk61")
{{- else if eq .chezmoi.os "darwin" -}}
  input (iokit-name "TMKB GK61")
  output (kext)
{{- end }}
  fallthrough true
  allow-cmd true
)

(defsrc
  esc   1    2    3    4    5    6     7    8     9    0    -     =     bspc
  tab   q    w    e    r    t    y     u    i     o    p    [     ]     \
  caps  a    s    d    f    g    h     j    k     l    ;    '     ret
  lsft  z    x    c    v    b    n     m    ,     .    /     rsft
  lctl  lmet lalt           spc             ralt  cmp  rctl
)

(deflayer qwerty
  grv   1    2     3    4     5    6     7    8     9    0    -    =    bspc
  tab   q    w     e    r     t    y     u    i     o    p    [    ]    \
  @cesc a    s     d    f     g    @test j    k     l    ;    '    ret
  lsft  z    x     c    @micv b    n     m    ,     .    /    rsft
{{- if eq .chezmoi.os "linux" }}
  lctl  lmet lalt             @snav           @dalt cmp  rctl
{{ else if eq .chezmoi.os "darwin" }}
  lctl  lalt lmet             @snav           @dalt XX   rctl
{{ end -}}
)

(deflayer nav
  _     F1   F2   F3   F4   F5   F6    F7   F8    F9   f10   F11   F12   _
  _     _    _    _    _    _    _     _    _     _     _    _     _     _
  _     _    _    _    _    home left  down up    right end  _     _
  _     _    _    _    _    _    _     _    _     _     _    _
  _     _    _              _               _     _     _
)

{{ if eq .chezmoi.os "darwin" }}
(deflayer acentos
  _     _    _     _    _     _    _    _    _     _      _    _     _     _
  _     _    _     @é   _     _    _    @ú   @í    @ó     _    _     _     _
  _     @á   _     _    _     _    _    _    _     _      _    _     _
  _     _    _     _    _     _    @ñ   _    _     _      _    _
  _     _    _                _              _     _      _
)
{{ end }}

(defalias
  {{ if eq .chezmoi.os "linux" -}}
  mic (cmd-button "pactl set-source-mute @DEFAULT_SOURCE@ 0"
                  "pactl set-source-mute @DEFAULT_SOURCE@ 1")
  {{ else if eq .chezmoi.os "darwin" -}}
  mic (cmd-button "osascript -e 'set volume input volume 80'"
                  "osascript -e 'set volume input volume 0'")
  {{ end -}}
  micv (tap-hold 200 v @mic)
  cesc (tap-next-release esc ctl)
{{ if eq .chezmoi.os "darwin" }}
  ace (layer-toggle acentos)
  dalt (tap-hold-next 1000 bspc @ace :timeout-button bspc)
{{ else if eq .chezmoi.os "linux" }}
  dalt (tap-hold-next 1000 bspc ralt :timeout-button bspc)
{{ end }}
  nav (layer-toggle nav)
  snav (tap-next-release spc @nav)
  test (tap-hold-next 1000 h lsft :timeout-button h)
{{ if eq .chezmoi.os "darwin" }}
  á (tap-macro A-e a)
  é (tap-macro A-e e)
  í (tap-macro A-e i)
  ó (tap-macro A-e o)
  ú (tap-macro A-e u)
  ñ (tap-macro A-n n)
{{ end -}}
)

;; (template
;;   _     _    _     _    _     _    _     _    _     _     _    _     _     _
;;   _     _    _     _    _     _    _     _    _     _     _    _     _     _
;;   _     _    _     _    _     _    _     _    _     _     _    _     _
;;   _     _    _     _    _     _    _     _    _     _     _    _
;;   _     _    _                _               _     _     _
;; )

;; vi: ft=lisp
