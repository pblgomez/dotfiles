local opts = { noremap = true, silent = true }
local keymap = vim.keymap.set

-- LeaderKey to Space
vim.g.mapleader = " "
keymap('n', '<leader>e', ':Lexplore! 25<CR>')
keymap("n", "<space>", "<Nop>", { noremap = true, silent = true }) -- Disable spacebar

keymap('n', '<leader>c', ':nohlsearch<CR>', opts) -- Clear selections

-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- Navigate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)
keymap("n", "<S-x>", ":bdelete<CR>", opts)

-- Move paragraphs
keymap("v", "J", ":m '>+1<CR>gv=gv")
keymap("v", "K", ":m '<-2<CR>gv=gv")

-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Keep cursor in the same place when useing 'J'
keymap('n', 'J', 'mzJ`z')

-- Paste without yank
keymap('x', '<leader>p', '\"_dp')
-- Delete without yank
keymap('n', '<leader>d', '\"_d')
keymap('v', '<leader>d', '\"_d')

-- Yank to clipboard
keymap('n', '<leader>y', '\"+y')
keymap('n', '<leader>Y', '\"+Y')
keymap('v', '<leader>y', '\"+y')

keymap('n', '[d', vim.diagnostic.goto_prev)
keymap('n', ']d', vim.diagnostic.goto_next)
