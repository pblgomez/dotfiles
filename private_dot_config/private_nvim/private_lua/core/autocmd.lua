-- ExtraWhitespaces
vim.cmd([[ autocmd BufWritePre * :%s/\s\+$//e ]])

-- Formatters
vim.cmd([[ autocmd BufWritePre *.py,*.lua,*.sh lua vim.lsp.buf.format({ async = false }) ]])

-- TODO: Disable Lsp on helm
--                vim.api.nvim_command('LspStop')
