local options = {
  -- General
  autowrite      = true,     -- Write the contents of the file on other commands
  -- clipboard = "unnamedplus", -- allows neovim to access the system clipboard
  colorcolumn    = "80,160", -- Marks columns 80 and 160
  cursorline     = true,     -- highlight the current line
  ignorecase     = true,     -- ignore case in search patterns
  laststatus     = 3,        -- Global statusline
  number         = true,     -- set numbered lines
  relativenumber = true,     -- set relative numbered lines
  undofile       = true,     -- enable persistent undo
  smartcase      = true,     -- no automatic ignore case switch
  smartindent    = true,     -- make indenting smarter again
  swapfile       = false,    -- creates a swapfile
  wrap           = false,
  -- FOLD
  -- foldmethod     = "marker", -- Use markers as folds
  -- Folding based on treesitter
  foldmethod     = "expr",
  foldexpr       = "nvim_treesitter#foldexpr()",
  foldenable     = false,
  -- Tabs
  expandtab      = true, -- convert tabs to spaces
  shiftwidth     = 2,    -- the number of spaces inserted for each indentation
  shiftround     = true, --  use multiple of shiftwidth when indenting with '<' and '>'
  softtabstop    = 2,    -- insert spaces for a tab
  tabstop        = 2,    -- insert 2 spaces for a tab
  cmdheight      = 0,    -- Disable command line
}

for k, v in pairs(options) do
  vim.opt[k] = v
end
