-- TODO: Recheck docs
local status_ok, _ = pcall(require, "lualine")
if not status_ok then
  return
end

local hide_in_width = function()
  return vim.fn.winwidth(0) > 80
end

local diff = {
  "diff",
  colored = false,
  symbols = { added = " ", modified = " ", removed = " " }, -- changes diff symbols
  cond = hide_in_width
}

require('lualine').setup {
  options = {
    globalstatus = true,
  },
  sections = {
    lualine_a = {
      'mode',
      {
        'buffers',
        hide_filename_extension = true,
        max_length = 80,
      },
    },
    lualine_c = {},
  }
}
