require("null-ls").setup({
  on_attach = on_attach,
  sources = {
    -- Formatting
    require("null-ls").builtins.formatting.black.with({
      extra_args = { "--line-length", "160" }
    }),
    require("null-ls").builtins.formatting.isort,
    require("null-ls").builtins.diagnostics.mypy,
  },
})
