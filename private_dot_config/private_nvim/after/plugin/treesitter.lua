local status_ok, _ = pcall(require, 'nvim-treesitter')
if not status_ok then return end

require('nvim-treesitter.configs').setup {
    auto_install = true,
    highlight = {
        enable = true,
    }
}
