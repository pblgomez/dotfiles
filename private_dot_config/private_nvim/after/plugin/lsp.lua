local lsp = require('lsp-zero').preset({
    name = 'minimal',
    set_lsp_keymaps = true,
    manage_nvim_cmp = true,
    suggest_lsp_servers = false,
})

lsp.ensure_installed({
    'bashls',
    'lua_ls',
    'pylsp',
})

require("mason-tool-installer").setup({
    ensure_installed = { "stylua", "shfmt", "shellcheck", "black", "isort" },
    auto_update = true,
    run_on_start = true,
})

lsp.configure('lua_ls', {
    settings = {
        Lua = {
            diagnostics = {
                globals = { 'vim' },
            },
        },
    },
})

lsp.configure('pylsp', {
    settings = {
        pylsp = {
            plugins = {
                flake8 = {
                    maxLineLength = "160",
                },
                pycodestyle = {
                    ignore = { "W391" },
                    maxLineLength = 160,
                },
            }
        }
    }
})

lsp.setup()
