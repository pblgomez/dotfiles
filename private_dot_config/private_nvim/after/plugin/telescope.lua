local status_ok, _ = pcall(require, "telescope")
if not status_ok then return end

local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fh', function() builtin.find_files({ hidden = true }) end)
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
