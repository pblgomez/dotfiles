#!/usr/bin/env sh

wl-copy "$(grep -v '^#' ~/.local/share/bookmarks | wofi --show dmenu | awk '{print $1}')"
